# Specifying Systems   ($TLA^+$Language)

>为个人学习Lamport所著TLA语言书籍的笔记
>
>所有的#TODO为未总结(理解有困难)或待补充部分
>



## 1 简单数学基础

* 基础集合运算(交、并、补、差)
* 命题逻辑和谓词逻辑
  * 谓词逻辑中的全称量词$\forall$，称为无界量化，对应自由变量；存在量词$\exists$，称为有界量化，对应约束变量
  * $\forall$与合取相近， $\exists$与析取相近
* (补充第6章)关系$xRy$
  * 三个关系的性质：自反、(反)对称、传递
  * 偏序关系：自反、反对称、传递($\leq$)
  * 严格偏序：反自反、反对称、传递 ($<$)
  * 全序关系：偏序关系 + 完全的（$i.e. \; \forall x,y\in S,\; xRy\;\;\vee\;\;yRx=True$）
  * 良序关系：定义集合任意子集上该关系均有最小元

****



## 2 定义一个简单时钟

>  我们的任务：用$TLA^+$语言形式化地描述系统，后续使用$TLC$等工具进行模型的检查

**关于系统、行为等的定义**

* $def$    状态(state)： 为一组变量赋值的操作

* $def$    行为(behaviour)： <u>状态</u>序列

* $def$    系统(system)： 一组可能出现的<u>行为</u>的集合，每个行为表示系统一次正确的执行

* $def$    步骤(step)： 一对连续的<u>状态</u>，即从状态1转移至状态2

**为定义系统，我们需要在TLA中描述**

* 初始谓词(Initial predicate)
* 后继状态关系(next-state relation)
  * $def$    动作：含变量、后继变量(变量带 ' ，单引号的 ' )的公式，动作表示<u>步骤</u>的值为真或假
  * $def$    动作的执行：某个后继状态<u>动作</u>的<u>步骤</u>出现时，称此动作被执行(注：实质上公式不能被执行)
* 最终，以一个公式代表最终的规约(Specification)

<font color=purple>**语法方面**</font>

* 时态逻辑运算符 $\square$ ，$\square F$表示公式$F$恒为真，用于<u>动作</u>时，即说明该<u>动作</u>对<u>行为</u>的每一个<u>步骤</u>都为真
* $[nxt]_{variable}$ 写法等价于 $nxt \; \vee \space (variable' = variable)$，即允许出现重叠步骤 

****



## 3 异步接口示例

>采用两阶段握手协议的异步接口。
>
>即：发送方收到接收方的确认ack,才会继续发送消息。

**为了使用$TLA^+$描述(上方描述的)接口，我们需要做的**

1. 选择合理的抽象来描述系统，主要是：确定系统状态的变量、改变这些状态的步骤、步骤的粒度

2. 确定变量的范围，引入数据范围(Constant）、类型不变式(TypeInvariant)等，形式化地定义类型和不变式：

  * $def$     定理(theorem)：一个被所有<u>行为</u>满足的时态公式(带时态逻辑运算符)
  * $def$    状态函数(state function)：没有 ' 后继状态符号 和 $\square$ 时态逻辑运算符 的表达式，可含变量和常量
  * $def$    状态谓词(state predicate)：值域为布尔值的<u>状态函数</u>
  * $def$    不变式(invariant)：规约$Spec$中的不变式$Inc$是状态谓词，使得$Spec \Rightarrow Inc$ 为<u>定理</u>
  * $def$    类型(type)：变量$v$在规约$Spec$中具有类型$T$当且仅当$v \in T$是$Spec$的<u>不变式</u>

  最终规约中不会出现类型不变式，因其蕴含“   不变式的公式为<u>不变式</u>”

3. 定义初始谓词$Init$，为一个合取式

4. 定义后继状态动作$Next$，我们将其拆分为析取范式的形式，即：

  * $Next \triangleq A_1 \; \vee \; A_2 \; \vee \; A_3 \; ...\; \vee \; A_n$
  * $A_i \triangleq condition_1 \; \wedge \; ...\; \wedge \; conditon_p \; \wedge \; next\_state_1 \; \wedge \; ... \; \wedge \;next\_state_q$

  即：状态转移有$n$种方式，第$i$种方式满足的前置条件即使能条件有$p$条，而转移后状态对变量的赋值有$q$条

5. 规约式：$Spec \; \Rightarrow \; Init \;\wedge\;\square\;Next$
5. 定理式：<font color=blue>THEOREM</font> $Spec\;\Rightarrow\;\square TypeInvariant$
5. 为了模块解耦，减少暴露在外的变量，我们可以采用类似于结构体的写法，称作记录

<font color=purple>**语法及代码风格规范方面**</font>

1. 最外侧合取/析取式的 $\vee、\wedge$要求跨行书写，在行首且精确对齐，并省略括号，详见15章 15.2.2的要求

2. UNCHANGED关键字，表示变量在新旧状态下有相同的值

3. 元组表示为$<a,\;b,\;c>$，ASCII码为$<<$和$>>$，注意元组有序，允许重复值

4. **记录**(record)：可以具有不同的字段

   * 例：若$r$是类型，则$r.val$为字段，比如定义类型不变式 $ TypeInvariant\;channel\;\in\;[val: Data,\;rdy:\;\{0, 1\},\;ack:\;\{0, 1\}]$，注意字段的类型使用   :   而非 $\in$
   * 记录是无序的，改变声明顺序对其无影响
   * 描述记录在状态转移后的新值
     * 可以使用EXCEPT关键字，其后说明的值为新值，其余不变
     * ! 符号表示新记录
     * @ 符号表示状态转移前的旧值

5. <u>动作</u>也可写为带参数的形式，参数的条件需要在状态转移式中用存在量词等进行限定

6. 注释

   >$TLA^+$规约需要多写注释，例如对后继状态动作的说明等

   * 一行写满: ------------------ ， 装饰和分隔用，无其他含义
   * 行间注释： (*  这里是注释  *)，可以出现在任何地方，且允许嵌套
   * 行尾注释： \\*开头 

<font color=purple>**语言说明及特性**</font>

1. $TLA^+$是类型无关语言，类型不变式的含义为：满足<u>不变式</u>，亦即满足规约的<u>行为</u>中，变量取值满足的条件
2. * $def$    运算符(operator)：我们定义的<u>动作</u>中，其表达式左侧均为带0到多个参数的称为运算符
   * $def$    标识符(identifier)：我们称带至少一个参数的<u>运算符</u>为标识符
   * $def$    符号(symbol)：不带参数的标识符及类似$+$这样的内置运算符    
3. 符号的作用范围(scope)
   * VARIABLE CONSTANT关键字声明： 整个模块
   * EXTENDS 引入到： 整个模块
   * 标识符的参数： 该表达式内
   * 存在量词，全称量词：该量词的作用范围内

****



## 4 FIFO接口示例

>利用 3 中的两阶段握手协议异步接口，实现一个First in First Out 消息缓冲区，规定发送方进程、缓冲池、接收方进程
>
>即有两个通道：发送方和缓冲区的$in$通道， 缓冲区和接收方间的$out$通道
>
>有四种步骤：在两个通道上各自的$Send$和$Rcv$步骤

<font color=purple>**Sequences模块**</font>

定义了在元组上的演算  (定义有限序列为元组)

* Seq(S)        由S中任意元素生成的序列生成的集合
* Head(S)     第一个元素
* Tail(S)         去掉第一个元素的剩余序列
* Append(S, e) 将元素e追加到序列S尾部得到的序列
* s $\circ$ t            将s和t按照s先顺序连接得到的序列(ASCII表示为\\o   小写字母O)
* Len(s)         序列s的长度  

<font color=purple>**模块实例化的语法等**</font>

* 实例化一个引入的模块
  *  $v  \;\;\triangleq \;\;$<font color=blue>$INSTANCE$</font> $model$ <font color=blue>$WITH$</font> $d1 \leftarrow v1, d2 \leftarrow v2$
  *  另一个模块中声明未定义的数据范围及所有VARIABLE都需要通过WITH关键字给出
  * 实例的模块变量用 ! 给出
  * WITH 语句可在两名字相同时省略
* 实例化仅用于变量的隐藏

<font color=purple>**隐藏内部变量**</font>

* 时态逻辑的存在量词(existential quantification) <font bold=true>$\exist$</font> (应当加粗)
  * 对于某个行为， TLA写法：$\exist x\;:\;F$为真当且仅当存在一个值序列，即在行为的每个状态中都有一个值可以赋值$x$可以使得公式$F$为真
* 隐藏内部变量仅需要使用时态逻辑的存在量词满足$Spec$即可，但需要避免命名冲突
  * 故我们只需要定义新moudle，实例化并重写规约式，加入时态存在量词

<font color=purple>**其他语法**</font>

* ASSUME 关键字语句用于对常量的假设

**系统和环境，封闭和开放系统**

* $def$    环境(environment)：描述的系统（如我们描述FIFO缓冲区）的，和其对应操作即与其有交互的相关实体(如发送、接收方)
* $def$    开放系统(open-system)：只描述系统的正确行为
* $def$    封闭系统(closed-system or complete-system)：描述系统及其环境的正确行为

****



## 5 缓存示例

>多个处理器组成的多处理器系统，通过抽象接口与内存连接在一起
>
>通过约定接口等，我们的目标是：
>
>1. 实现可线性化内存(定义见下)的系统规约
>2. 基于 1. 实现直写式缓存的系统规约

**首先，为了实现可线性化内存系统，我们需要做的**

1. 可线性化内存系统：
   * 一个处理器$p$发出一个内存请求，在等到响应后才执行下一个请求
   * 允许请求和响应间的任意时间访问内存状态
   * 规约中，定义通过访问/读取/修改一个变量$mem$来执行请求
2. 定义内存接口，在这里我们需要约定消息的发送和接受接口、接口发送值(即  写请求  和  读请求 的集合)
   * 由于需要定义操作，故我们需要声明携带一到多个入参的常规运算符，采用CONSTANT修饰，并加以ASSUME限制
3. 确定记录处理器和请求、响应状态的变量等，确定响应状态变化序列(行为)
4. 按照引入包、类型正确不变式、初始谓词、后继状态动作析取式、各种状态转移步骤合取式、规约式、定理式的顺序编写规约

**接着，为了实现更复杂的直写式缓存(write-through)系统，我们需要做的**

1. 直写式缓存：

   * 每个处理器对应一个cache, 每次数据更新均无条件写入主存中
   * 处理器连接cache，每个cache和总线相连，总线另一端为主存，总线向主存写有FIFO等待队列

   <img src="./data/1.png" alt="image-20230425114405227" style="zoom:67%;" />

   

2. 如上图，我们规定$cache[p]$为处理器$p$的缓存，$buf[p]$为  当前请求的状态  或  当前请求的响应；同时我们规定$ctl[p]$为处理器的请求状态（做好准备响应下一条响应 rdy，正忙 busy，以及等待主存回写 waiting，等待队列memQ; 同时语义性地规定读请求$DoRd(p)$和写请求$DoWr(p)$,注意队列中读写的一致性、cache和主存中数据的一致性,避免读取到脏数据

3. 按照引入包、常量ASSUME限制、可线性化内存系统实例化、初始谓词、类型不变式、一致性谓词**Coherence**、请求动作合取式、下一状态转移析取式、规约式、定理式的顺序编写规约

<font color=purple>**函数**</font>

1. $def$    函数：函数$f$具有定义域 <font color=blue>DOMAIN</font> $f$，$f$将每个定义域中的元素$x$置为$f[x]$  (  数学中的$f(x)$  )
   * $def$    值域(range)：形如$f[x]$的值的集合，$x\in DOMAIN \;\; f$
   * 定义于  $S$, 值域为   $T$ 的子集  的所有函数的集合记作$[S \to T]$
   * $TLA^+$函数记法： $[x \; \in \;S \; \longmapsto \;e]$
   * 简写  $\sigma.c$   等价于  $\sigma[''c'']$      类似的有       $!.c$ 等价于 $![''c'']$
   * 函数对应编程语言中的数组，定义域对应于数组的索引集；定义域为一组函数的函数  对应于  其元素为一维数组的数组
   * 多入参函数的定义域为一个**元组集合**，类似于多维数组。实际应用中，往往用多个单入参函数代替多入参函数
     * 如 $f[5,3,1]$为$f[<5,3,1>]$的缩写形式
2. 记录是特殊的函数
   * 定义域：一个有限字符串集
   * EXCEPT 关键字的用法，为构造一个与原函数相同的新函数，除了更改EXCEPT后的映射关系
3. 元组是函数
   * 元组 $<a,\;b,\;c>$ 即函数：定义域$\{1,\;2,\;3\}$，映射关系$1\to a,\quad 2\to b,\quad3 \to c$
   * $<a,\;b,\;c>[2] = b$  注意下标从  1  开始
   * $TLA^+$提供笛卡尔积算子$\times$
4. Sequences的序列为函数
   * Sequences定义有限序列为元组
   * 长为$n$的序列即定义域为$1..n$的函数
5. 递归函数
   * 允许直接在0到多入参函数的定义式的右侧中使用正在定义的函数作为递归

<font color=purple>**其他语法**</font>

* CHOOSE关键字： $CHOOSE \; x:F$  表示满足公式$F$的任意<u>一个</u>值$x$(类似随机，固定种子)，若不存在满足的$x$则为完全随机的值
* IF .... THEN ... ELSE 语句     IF ...THEN...ELSE IF...THEN...ELSE语句
* LET...IN 构造
  * LET子句由一系列定义组成，并且这些定义可带参数，定义域(作用范围)直至IN子句句尾
  * 常见用法：使用LET语句所有表达式、分解公式使之更有层次性

<font color=brown>**从一个规约证明其实现了另一个规约**</font>

>  命题：在本章，我们在直写式缓存中的规约$Spec$实现了前述线性内存的规约$ISpec$
>
> 在$TLA^+$中，实现 等价于 蕴含， 另外系统描述 和 规约 等价
>
> 故前述命题即为直写式缓存的$Spec$实现了线性内存$ISpec$

如要给出类似数学的证明，有如下步骤：

1. 命题转化为：找到  证据 (witness)即可量化的 蕴含式右侧模块的变量，进行重命名代换，这种”证据“称为**转化映射**

   * 往证：$Spec$在转换映射下实现了$ISpec$

2. 模块$F$转化映射后的公式记为$\overline F$, 变量$x$记为$\bar x$，往证：$Init \; \wedge \; \square [Next]_{<a,b,c...>} \; \Rightarrow \; \overline{IInit} \; \wedge \; \square[\overline{INext}]_{<\overline a, \overline b, \overline c...>}$

   * 注：同一变量可不加$\quad\overline{\;\;\;\;}$

3. 往证 寻找一个$Spec$的不变式$Inv$：

   * $Init \Rightarrow \overline{IInit}$ 为真
   * $(Inv \; \wedge\; Next) \;\Rightarrow\; \overline{INext} \; \vee \; UNCHANED<\overline a, \overline b, \overline c...>$ 

   其中，第二个式子称为**步骤仿真**

* **TLC模型检查工具可以检查一个规约是否实现了另一个规约**

****



## 6 数学基础拓展

### 6.1 集合

1. $TLA^+$中定义运算符如下(内置运算符，不需要加括号即书写如下)：
* $UNION\;\; S$      并集
* $SUBSET\;\;S$     幂集 (S所有子集的集合)
* $FiniteSets$中定义的运算符
   * $Cardinality(S)$  基数运算，S中元素的个数
   * $IsFiniteSet(S)$  有限集时为真
2. 关于集合有如下表述：

* $\{x \in S:\; p\}$      S满足条件p的元素的集合，即为<u>一个</u>S的子集
* $\{e:\; x\in S\}$       由S中所有元素按照e计算生成的集合

3. 一些数学知识：

* 集合不能递归定义(集合生成集合则会产生罗素悖论)，具体定义为，设$C$为  元素是集合  的集合，现有操作$SMAP,s.t.$

  * 对于<u>所有</u>集合$S$，$SMAP(S)\in C$   (由集合生成集合的操作)
  * $\forall S, T \;\;and \;\; S \neq T, SMAP(S) \neq SMAP(T)$

  则$C$不是一个集合

### 6.2&6.6 Siily Expressions

1. $TLA^+$语言中，只要求语法格式正确，即允许 / str 这种语义错误的句子(类比不进行编译器中的语义检查，只进行语法分析)
2. 一些未定义行为：
   * 定义一个不可能存在的函数：生成一个固定但的函数不可知的函数
   * CHOOSE语句后有多个甚至无穷多个满足条件的判定时：生成一个固定不可知的满足条件的值
   * 运算符操作数为预期外的“类型”，如$Tail(\frac{1}{2})$，有固定未知值
3. $TLC$模型检查器在计算语义错误的表达式时会报错

### 6.3 递归及函数的定义

1. 函数的定义式的两种表达：

   * $f[x\in S] \;\;\triangleq\;\; e$
   * $f \;\;\triangleq\;\; CHOOSE \;\; f: \; f=[x\in S \longmapsto e]$

   这也即递归函数符号可在等式右侧出现

2. $TLA^+$不允许循环定义，既不允许$f$的定义中调用$g$的定义，又在$g$的定义中调用$f$的定义

   * 通过声明两函数为一个记录的两个字段来绕开此问题

3. 为了避免定义“不存在”的递归函数，必须关心右侧定义的**唯一性**(数值计算不需要过于关注)

### 6.4-6.5 函数与运算符

1. 函数：表示值的完整表达式； 运算符不带操作符无语义且不合语法

2. 函数：允许递归定义； 运算符不允许递归定义

   * 通过LET...IN子句嵌套定义一个递归函数来编写需要递归的运算符

3. 运算符允许将(其他)运算符入参,例如$PartialOrder(\_<\_,S)$，即第一个参数是$<$运算符，第二个参数是集合

4. 注：类似于SUBSET并非一个函数，因为其没有定义域(集合的套娃定义)，其是一种特殊对象

5. 注意以下两个函数：

   * $f'=[i\in Nat \longmapsto i+1]$
   * $\forall i\in Nat :\; f'[i]=i+1$

   两者区别：前者定义域为$Nat$，后者未给出，定义域包含$Nat$； 事实上后者甚至未必是函数

   习惯采用前者写法

****



## 7 #TODO编写规约的一些建议

待8-11章读后补充完整规约书写注意事项

****



## 8  活性及公平性

>如何定义系统"需要"出现的行为，及如何限定动作"读取后执行"的"执行"操作
>
>弱公平性：一旦(在不发生对应动作下的永久)满足使能条件，动作最终会发生
>
>强公平性：一旦(在不发生对应动作下的无限多次，$\square \Diamond$ 满足使能条件)，动作最终会发生
>
>$example$:  时间片轮转进程调度中，运行的进程必须能够分配到时间片资源，即下述的$\square (\square \text{ENABLED}(A) \Rightarrow \Diamond A)$

### 8.1 时态公式

**时态公式说明**

* 记行为$\sigma$为$\sigma_0 \to \sigma_1 \to \sigma_2 \to \dots$, 则：

  $\sigma ^{+n} \quad \triangleq \quad \sigma_n \to \sigma_{n+1} \to \sigma_{n+2}\to \dots$

* 若$A$为一个<u>动作</u>，则$\sigma \models A$表示$\sigma_0 \to \sigma_1$为$A$步骤

* $\sigma \models F$ 

  1. 含义  时态公式$F$，表征<u>行为</u>$\sigma$值为布尔值的表达式
  2. 定义
     * $\sigma \models (F\wedge G) \quad \triangleq \quad (\sigma \models F) \wedge (\sigma \models G)$
     * $\sigma \models \neg F \quad \triangleq \quad \neg (\sigma \models F)$
     * 由于$\{\neg, \wedge\}$为极小完全集，故$\sigma \models F_1 \; op \; F_2$  均等价与 $(\sigma \models F_1 )\;op\;(\sigma \models F_2) $
     * $\sigma \models (\exists r: F) \quad \triangleq \quad \exists r: (\sigma \models F)$
     * $\sigma \models (\forall r \in S: F) \quad \triangleq \quad \forall r \in S: (\sigma \models F)$ 

**符号说明(符号的推导见8.10.1)**

* $\square$ (always) 

​		$\sigma \models \;\square F \quad \triangleq \quad \forall n \in Nat: \sigma^{+n} \models F$

* $\Diamond$ (eventually)   

​		$\Diamond F  \quad \triangleq \quad \neg \square \neg F$

​        $\sigma \models \Diamond F \quad \equiv \quad \exists n \in Nat: \sigma^{+n} \models F$

* $\leadsto$ (lead to)

​		$F \leadsto G \quad \triangleq \quad \square(F \Rightarrow \Diamond G)$    

​		$\sigma \models (F \leadsto G) \quad \equiv \quad \forall n \in Nat: (\sigma^{+n} \models F) \Rightarrow (\exists m \in Nat: (\sigma^{+(+n+m)} \models G))$

* $\Diamond \langle A \rangle _v$

​		$\Diamond \langle A \rangle _v \quad \triangleq \quad \neg \square[\neg A]_v$

​		$\langle A \rangle _v \quad \triangleq \quad A \wedge (v' \neq v)$

* $\square\;{\Diamond} F$ : infinitely often 无限重复次真

​		$(\sigma \models \square \Diamond F) \quad \equiv \quad (\exists _{\infty} \; i \in Nat: \; \sigma ^{+i} \models F)$， 其中：

​		$(\exists _{\infty} \; i \in Nat:\; P(i)) \quad \equiv \quad (\forall n \in Nat:\; \exists m \in Nat:\; P(n+m))$

* $\Diamond\;\square F$ : eventually always 某个时刻之后均为真  

**优先级**

$\{\square \; \Diamond \}\quad > \quad \{ \wedge \; \vee \} \quad > \quad \{ \leadsto \}$ 

**重叠不变性**(invariant under stuttering)

* $def$    重叠步骤:  步骤的前后两个状态中，变量保持不变。可表征系统的的其他部分发生了一些变化，但没有被当前公式表征出来

* $def$    (时态公式的)重叠不变性: 增加或删除一个重叠步骤不会影响$\sigma \models F$的真值

  * <u>状态谓词</u>可视为重叠不变式，由于其真值仅由$\sigma_0$决定
  * 公式$^{_\square }A_v$对任意 <u>动作</u> $A$ 和 <u>状态函数</u> $v$ 都是重叠不变式，像上面这样应用$^{_\square} $及组合逻辑运算符得到的公式均为重叠不变式()

  注： $TLA^+$只研究是重叠不变式的时态公式

### 8.2-8.3 时态重言式&时态证明规则

>包含时态运算符的恒真式称为时态重言式

**常用时态重言式（部分推导见8.10.2）**

* $\neg \square F \quad \equiv \quad \Diamond \neg F$
* $\square (F \wedge G) \quad \equiv \quad \square F \wedge \square G$
* $\Diamond (F \vee G) \quad \equiv \quad \Diamond F \vee \Diamond G$
* $\square F \vee \square G \quad \to \quad \square(F \vee G)$
* $\Diamond F \wedge \Diamond G \quad \to \quad \Diamond (F \wedge G)$
* $\square \Diamond (F \vee G) \quad \equiv \quad (\square \Diamond F) \vee (\Diamond \square G)$
* $\Diamond \square (F \wedge G) \quad \equiv \quad (\Diamond \square F) \wedge (\Diamond \square G)$
* $[A \wedge B]_v \quad \equiv \quad [A]_v \wedge [B]_v$
* $\langle A \vee B \rangle _ v \quad \equiv \quad \langle A \rangle _v \vee \langle B \rangle _v$

注：区分 $[\quad]_v$ 和 $\langle \quad \rangle _v $  ,  前者是允许$v$不改变的步骤, 后者是所有步骤必须改变$v$

**时序逻辑的对偶式**

* 命题逻辑中的对偶定理
  * 对偶式：$\vee$ 替换为 $\wedge$,  $\wedge$ 替换为$\vee$,  $0$(不是变量，是公式中的真值$0$)替换为$1$, $1$替换为$0$
  * 对偶定理：若$A \iff B$ 则对偶式 $A^* \iff B^*$
* 时态逻辑中的对偶
  * 对偶式：除了命题逻辑中的替换外，还需要将$\square$替换为$\Diamond$、$\Diamond$替换为$\square$
  * 对偶定理：时态重言式的对偶式也是重言式(猜测：有类似命题逻辑的表述)

**时态证明规则**

* 所有命题逻辑的证明规则
* **通用规则**

​		$\forall F, \text{We have } F \quad \Rightarrow \quad \square F$

* **蕴含通用规则**

​		$\forall F,\; G, \text{and } F \to G \text{  We have } \square F \quad \Rightarrow \quad \square G$

### 8.4  弱公平性

#### 8.4.1 弱公平性的定义

**（规约的）安全属性及活性属性**

1. $def$    安全属性：系统不能做的事情
2. $def$    活性属性：系统需要做的事情

**区分公式和动作**

1. 时态公式表征的是<u>行为</u>，即包含$\sigma_1, \sigma_2 \dots$的状态序列
2. 动作表示的是<u>步骤</u>

<font color=purple>**弱公平性$WF$(Weak Fairness)**</font>

* $def$    :$ENABLED \; A \;$是一个谓词，当且仅当满足动作$A$发生条件(动作合取式前部不带 ' 的条件)的状态$\sigma_i$下为真

* $A$上的弱公平性$WF_v ( A )$ 等价于： $\square (\square \text{ENABLED} \langle A \rangle_v \Rightarrow \Diamond \langle A \rangle_v )$
* 上式等价于：$\square \Diamond (\neg \text{ENABLED}\langle A \rangle_v) \; \vee \; \square \Diamond \langle A \rangle _v$
* 上式等价于：$\Diamond \square (\text{ENABLED} \langle A \rangle _v ) \Rightarrow \square \Diamond \langle A \rangle_v$
* 三个式子的含义分别为：
  * $A$若曾经被永久使能，则$A$最终会出现
  * 要么$A$被无限次去使能，要么有无限个$A$步骤出现
  * 如果$A$最终被永久使能，则有无限个$A$步骤会出现
  * **推导见8.10 推导补充(手写) 8.10.13**

#### 8.4.2 弱公平性的含义及相关等价性

1. 记$ENABLED \langle A \rangle _v$ 为 $E$, $\langle A \rangle_v$为$A$，下证何时我们书写的规约蕴含了$WF_A \equiv \square(ENABLED\langle A \rangle_v \Rightarrow \Diamond \langle A \rangle _v)$
2. 即：弱公平性本来的表述：若$A$被永久使能，则$A$必会最终发生；现在证明在某些条件下其等价于：一旦$A$有序列中某个状态被使能，则$A$必会最终发生
3. 给出两个式子等价的$spec$所需条件：$\square (E \Rightarrow \square E \; \vee \; A)$   **即：一旦被$A$使能，除了动作$A$没有其他动作可去使能**

* 即有时态重言式：$\square (E \Rightarrow \square E \; \vee \; A) \quad \Rightarrow \quad (\square(E \Rightarrow\Diamond A) \equiv \square(\square E \Rightarrow \Diamond A))$

* **该重言式证明见8.10.4**

### 8.5 弱公平性合取规则&量词规则

* $A_1, A_2 \dots A_n$为动作，定义式$DR(i, j) \quad \triangleq \quad \square( ENABLED \; \langle A_i \rangle_v \Rightarrow \neg ENABLED \langle A_j \rangle_v \; \vee \; \Diamond \langle A_i \rangle _v)$

**弱公平性合取规则**

$(\forall i, \; j \in \{1, 2, 3\dots n\}:(i \neq j) \;\Rightarrow\; DR(i,j))\quad \Rightarrow \quad (WF_v(A_1) \;\wedge\;\dots\;\wedge\;WF_v(A_n) \;\equiv\;WF_v(A_1 \;\vee\;\dots\;\vee\;A_n))$

**弱公平性量词规则**

$(\forall i, \; j \in \{1, 2, 3\dots n\}:(i \neq j) \;\Rightarrow\; DR(i,j)) \quad \Rightarrow \quad (\forall i \in S:\;WF_v(A_i) \;\equiv\; WF_v(\exists i \in S: \;A_i))$

**上述规则的两变量(即 $i,j \in \{1, 2\}$ 的证明见8.10.5，两变量可轻易拓展至多变量)**

### 8.6 强公平性

<font color=purple>**强公平性$SF$(Strong Fairness)**</font>

$A$上的强公平性$SF_v(A)$定义为：

* $\Diamond \square (\neg \text{ENABLED}\langle A \rangle_v) \;\vee\; \square\Diamond\langle A\rangle_v$
* $\square \Diamond \text{ENABLED} \langle A\rangle_v \Rightarrow \square \Diamond \langle A \rangle_v$

两式等价性证明基本同弱公平性等价性的证明(8.10.4)

**强弱公平性等价条件**

$(WF_v(A) \equiv SF_v(A)) \quad \equiv \quad (\square \Diamond (\neg \text{ENABLED} \langle A \rangle_v) \;\Rightarrow\; \Diamond\square(\neg \text{ENABLED} \langle A \rangle_v) \;\vee\; \square \Diamond \langle A \rangle _v)$

>如果$A$被无限次禁止使能，则$A$最终会被永久去使能，或者最终发生$A$
>
>一种理解为：一旦使能条件满足，只有发生该动作，才能去使能；即：一旦被使能，就一直被使能，直到其执行
>
>强弱公平性等价时，使用弱公平性描述

**强公平性合取规则**

$(\forall i, \; j \in \{1, 2, 3\dots n\}:(i \neq j) \;\Rightarrow\; DR(i,j))\quad \Rightarrow \quad (SF_v(A_1) \;\wedge\;\dots\;\wedge\;SF_v(A_n) \;\equiv\;SF_v(A_1 \;\vee\;\dots\;\vee\;A_n))$

### 8.8 时态公式量化

**在常量集合上的有界量化**

1. $\sigma \models (\forall r \in S:\; F) \quad \triangleq \quad (\forall r \in S: \; \sigma \models F)$
2. $\sigma \models (\exists r \in S:\; F) \quad \triangleq \quad (\exists r \in S: \; \sigma \models F)$

$F$ 式中的 $r$ 声明为常量，即$\sigma_i$中的$r$值相同

**时态存在量词$\boldsymbol {\exists}$**

目前，我们将$\boldsymbol \exists x:\;F$视作隐藏了$x$的$F$

$def$    $\boldsymbol \forall x:\;F \quad \triangleq \quad \neg \boldsymbol \exists x \neg F$ 

### 8.9 时态公式剖析

目前我们的规约通用形式：

$Spec \quad \triangleq \quad Init \;\wedge\; \square [Next]_{vars} \;\wedge\; Liveness$

其中，$Liveness$是有关$WF_v(A) \;\; SF_v(A)$的合取/析取式

**闭包**

$def$    子动作：$A$为$Next$子动作当且仅当$A \Rightarrow Next$（也有更弱的定义）

$def$    闭包： 当且仅当$Liveness$合取式既不限制初始状态，也不限制其他$Next$步骤出现

$def$    (更正式的)闭包：

* $def$   安全属性： 令一个有限行为是一个有限状态序列，有限行为$\sigma$满足一个安全属性$S$  当且仅当  由在$\sigma$后添加无限多个重叠步骤组成的行为满足$S$
* 公式对$(S,\; L)$满足闭包  当且仅当  每个满足$S$的行为可以被拓展为一个满足$S \wedge L$的无限行为

> 1. 不写不是闭包的规约
> 2. 若能保证$Liveness$是$Next$的子动作上 强/弱公平性公式的合取式，保证本节前述规约是一个闭包

**公平性公式上的转化映射(略)**

### 8.10 推导补充(手写)

[见：手写文档](https://gitlab.com/thu_practice/thu_practice_jbt/-/blob/notes/%E6%8E%A8%E5%AF%BC%E8%A1%A5%E5%85%85.pdf)

****



## 9 实时系统（Real Time）

### 9.2 通用实时规约

>通过定义以下模块$RealTime$，实现实时规约

1. 表示时间的变量：$now$， 类比与高级语言中自1970年开始到此刻的秒数

2. $RTNow(v)$  

   * $v$是一个状态函数
   * $NowNext(v) \quad \triangleq \quad now'\in \{r\in Real:\; r>now\}\;\wedge \; (v'=v)$
   * $RTNow(v) \quad \triangleq \quad (now \in Real) \quad \wedge \quad \square[NowNext]_{now} \quad \wedge \quad \forall r \in Real:\; WF_{now}(NowNext \wedge (now' > r))$
   * 注：更标准的写法可能要引入$LET\dots IN$子句

   该式断言：$now$在状态函数$v$改变时不变，在$v$不变时增长，且是无限制增长(这是公平性$WF$所定义的（时间的自增）

3. $RTBound(A,\;v,\;\delta,\; \epsilon)$   ----- $A$为一个动作

   * $TNext \quad \triangleq \quad t'  = \quad IF\quad \langle A \rangle_v \; \vee\; \neg(\text{ENABLED}\langle A \rangle _v)'\quad THEN \quad 0 \quad ELSE \quad t + (now' - now)$
   * $Timer(t) \quad \triangleq \quad (t = 0) \;\wedge\; \square[TNext(t)]_{t, v, now} $
   * $MaxTime(t) \quad \triangleq \quad \square(t \leq \epsilon)$
   * $MinTime(t) \quad \triangleq \quad \square[A \; \Rightarrow \; (t \geq \delta)]_v$
   * $RTBound(A,\;v,\;\delta,\; \epsilon) \quad \triangleq \quad \boldsymbol{\exists} t:\; Timer(t) \; \wedge MaxTimer(t) \;\wedge \; MaxTime(t) \;\wedge \; MinTime(t)$

   该式断言：$t$为上一次$A$去使能或发生$A$的时间，规定$t$必须介于一个上下限间；即：$A$使能必须大于$\delta$才能出现，且$A$的使能时间必须小于$\epsilon$    (#TODO:有无可能出现小于$\epsilon$被去使能的情况)

**在规约中加入实时约束的方法：** $Spec \; \wedge \; RTnow(v) \;\wedge\; MinTime(A, v, \delta, \epsilon)$

### 9.3 实时缓存

>除了“满足实时约束”这一条件本身之外，我们也需要关注如何满足实时条件约束
>
>在 5 中示例的直写式缓存中，可以添加一个有关实时算法的规约——轮询调度

* 对一些独立动作：$A_1, A_2\dots A_k$， 当：

  * 任两个动作不会同时使能
  * 任一个动作使能后才能使能另一个动作

  则  $RTBound(A_i,\; \_,\; \_,\; \_) \;\wedge \; \dots \; \wedge \; RTBound(A_n,\; \_,\; \_,\; \_) \equiv RTBound(A_1 \vee A_2 \dots A_n,\; \_,\; \_,\; \_)$

### 9.4 $Zeno$ 规约

1. $NZ$： 蕴含变量增长不受限的式子         $Example:$     $\forall r \in Real:\; WF_{now}(Next\; \wedge (now' > r))$

2. $def$    $Zeno$规约： 存在一个满足规约的安全部分的有限行为$\sigma$， 但不能将其扩展为同时满足安全部分和$NZ$的无限行为的规约

3. 一个规约是 <u>非$Zeno$规约</u> 当且仅当 其为<u>闭包</u>

4. 对带的有限集的$RTBound(A_i,\;, vars, \; \delta_i,\; \epsilon_i)$ 和 $RTNow(vars)$公式，加上$Init \; \wedge \square[Next]_{vars}$的公式

   * $0 \leq \delta_i \leq \epsilon \leq Infinity$
   * $A_i$是后继状态$Next$的子动作
   * $\forall i, j, \;\; and \;i \neq j,$ 没有步骤既是$A_i$步骤也是$A_j$步骤

   则规约是非$Zeno$的，也即闭包

   >将一个不是$Next$子动作的公式注入 $RTBound$ 极易得到$Zeno$规约，即非闭包

### 9.5 混合系统规约

1. $def$    混合系统规约：表示连续变化量的规约，与一般规约对应的离散形成对比
2. 与混合系统规约对应的是$DiffertialEquations$模块，其中包含类似求解微分方程的运算符$Integrate$
   * 形式：    $Intergrate(D,\; t_0,\; t_1,\; \langle x_0,\; x_1, \;\dots\; x_{n-1} \rangle)$
     * $D$     $D[t,\; \frac{dx}{dt}, \; \frac{d^2x}{dt^2}, \; \dots \; \frac{d^nx}{dt^n}] = 0$ 即为所求解的微分方程
     * $t_0$     初始时刻
     * $t_1$      所求时刻
     * $\langle x_0,\; x_1, \;\dots\; x_{n-1} \rangle$      $t_0$时的函数及其各阶导数
     * 所求目标：$t_1$时刻的函数及其各阶导数



****



## 10 组合规约

### 10.1 双规约的组合

1. $def$    交错规约：每个 <u>步骤</u>仅代表单个组件的单次操作
2. $def$    非交错规约：允许两个组件同时执行动作的规约

>现在考虑两个单变量规约（如时钟），我们将其组合为具有单一动作状态的整体

* $Init1 \;\wedge\; \square[Trans_1]_{v1} \;\wedge\; Init2 \;\wedge\; \square[Trans_2]_{v2}$ 

$\iff \;(Init1 \;\wedge\; Init2) \; \wedge\; \square([Trans_1]_{v1} \;\wedge\; [Trans_2]_{v2})$ 

$\iff\;(Init1 \;\wedge\; Init2) \; \wedge\; \square((Trans_1 \;\vee\;(v1'=v1))\;\wedge\; (Trans_2 \;\vee\; (v2'=v2))$

$\iff (Init1 \;\wedge\; Init2) \; \wedge\; \square[(Trans_1 \;\wedge\; Trans_2) \;\vee\; (Trans_1 \;\wedge\;(v2'=v2)) \; \vee \; (Trans_2 \;\wedge\; (v1'=v1))]_{<x,y>}$ 

>到此为止，是一个非交错规约，将其写为交错规约有如下两种方式

1. $Trans_1'\quad \triangleq \quad Trans_1 \;\wedge\;(v_2'=v_2)$

   $Trans_2'\quad \triangleq \quad Trans_2 \;\wedge\;(v_1'=v_1)$

2. 前式合取$\square [(x'=x)\;\vee\;(y'=y)]_{<x,y>}$

以上所做工作即如何去掉非交错规约的$Trans_1 \;\wedge\; Trans_2$项

### 10.2 多规约的组合

**组合规则**

对于任意集合$C$, 我们假设$(\forall k \in C: v_k'=v_k) \equiv (v'=v)$

则对于某些动作$F_{ij}$, 我们有：

​			$\begin{flalign} &(\forall k \;\in\; C: \; I_k \;\wedge\;\square [N_k]_{v_k}) \;\equiv\;\\ &\qquad \wedge\; \forall k \; \in \; C:\; I_k \\ & \qquad \wedge \; \square[\; \exists k \; \in C: \; N_k \;\wedge \; (\forall i \; \in \; C \;-\; \{k\}:\; v_i'=v_i) \\ &\qquad \qquad \; \vee \; \exists i, \; j \; \in \; C: \; (i \neq j) \;\wedge\;N_i \;\wedge\; N_j \; \wedge \; F_{ij}]_v \end{flalign}$

若使其其为交错规约，我们可以有：

1. 约定：对所有的$v_i \neq v_j, \;\;  N_i \; \Rightarrow v_j'=v_j$
2. 在规约中注入全局条件：$\square [\;\exists k \;\in C:\; \forall k \;\in\; C \; - \; \{k\}: \; v_i' = v_i]_v$



>  以下我们尝试：改写多个时钟组合的交错规约，主要关注语法上的函数定义及其合法与否的问题

1. 注意函数的定义/声明需要至少指出定义域，直接指定函数为: $\forall i \;\in\; Nat:\; f'[i]=i+1$  是错误的，因为除了定义域外甚至未指出$f'$是一个函数
2. 方法一：加入定义
   * $hrfcn \quad \triangleq \quad [k \; \in \; Clock \longmapsto hr[k]]$
   * $hrfcnc'\quad \triangleq \quad [hrfcn \; \text{EXCPET} \; !\;[k]=(hr[k] \% 12)+1]$
3. 方法二：
   * $IsFcnOn(f, S) \quad \triangleq \quad f =[x \;\in\; S \longmapsto f[x]]$
   * $Spec$中注入：$\square IsFcnOn(hr, Clock)$， 声明$hr$为一个定义域为$Clock$的函数
   * 子动作$N_k \quad \triangleq \quad hr' = [hr \;\; \text{EXCEPT} \;\; ![k] = (hr[k] \% 12) + 1]$

### 10.3 FIFO

> 将第4章中实现的异步握手接口改写为： 发送方、缓冲区、接收方三个组件的组合
>
> 我们分别按照交错和非交错规约实现

1. 首先确认各组件的状态函数：
   * 发送方： $<in.val, \; in.rdy>$
   * 缓冲区： $<in.ack, \; q, \; out.val,\; out.rdy>$
   * 接收区： $out.ack$
2. 改写$InnerFIFO.tla$前述规约，约定$Sender, \; Buffer,\; Reciever$三个组件的规约，最终合取
3. 为隐藏$q$队列，采用内部封装$InnerBuf$模块，外部用$\boldsymbol{\exists}$实例化
4. 类似于类型不变式，我们需要声明$in, \;out$两个变量是$Channel$
5. 注意原先的初始谓词有些是跨模块的，需要在顶部规约中独立写出好

### 10.4 共享状态的组合

#### 10.4.1 共享状态 

1. $def$    非相交状态组合(disjoint-state compositions)： 各组件由系统中独立的部分表示，每个组件的后继状态动作均描述了其对应的系统子状态的更改

   * 违背该条件的例子：系统的部分状态无法被分解至不同的组件,**系统执行过程中带来的状态变更完全在一个规约中描述，**一个简单的例子即两个到多个部件共享一个队列$q$

2. **共享状态组合规则**

   符号说明：组件$k$有后继状态动作$N_k$，所有组件共享一组变量$w$， 动作$\mu_k$指共享变量$w$中所有不属于组件$k$的变更，也即其他组件后继变量更改的部分，$v_k$为$k$组件的私有状态, $v$即所有组件私有变量$v_k$组成的元组

   以下四个条件共同蕴含最终结论：

   1. $(\forall k \;\in\; C:\; v_k'=v_k) \;\equiv\;(v'=v)$
   2. $\forall i, \; k \; \in \;C:\; N_k \;\wedge\; (i \neq k) \Rightarrow (v_i' = v_i)$       即：我们书写的是一个交错规约
   3. $\forall i, \; k \; \in \;C: \; N_k \;\wedge\; (w' \neq w)\; \wedge\;(i \neq k) \;\;\Rightarrow \mu_i$
   4. $(\forall k \;\in \; C:\;\mu_k) \;\equiv\; (w'=w) $ 

   * **结论(以上四式蕴含)：**

   $ (\forall k \;\in\; C:\; I_k\;\wedge\; \square[N_k \;\vee\; (\mu_k \;\wedge\; (v_k'=v_k))]_{<w, v_k>}) \; \equiv \; (\forall k \;\in\; C:\;I_k) \;\wedge\; \square[\;\exists k \;\in\; C:\;N_k]_{<w, v>}$

#### 10.4.2 相交动作组合

1. $def$    相交动作：组件之间有的动作需要"一起"发生，例如环境和系统分开建模时，两者的交互

2. 在课本中，给出了将可线性化内存拆分为系统—环境进行建模的例子，处理器向内存发送请求、内存做出响应均为相交动作，我们将其拆分为：

   * $MRqst、ERqst$  共同执行之前的$Rqst$操作
   * $MRsp、ERsp$     共同执行之前的$Rsp$操作

   我们让动作的发起方保持常使能(不设置condition条件)，书写规约最终进行合取即可

   注：为何这种写法不会使常使能方由于一直高电平而在常使能动作中乱跳：有共享变量表征了下一步的行为，在线性化内存例中变量为$memInt$

### 10.5 简短回顾

**组合方法分类**

1. 交错规约、非交错规约

   * 交错规约：每个(非重叠)步骤仅能被赋值给一个组件的规约
   * 非交错规约：允许一个补助同时表征多个组件的**并发**操作

2. 非相交状态、共享状态

   * 非相交状态规约：系统整体状态可被分割至每个单独组件的规约
   * 共享状态规约：

3. 相交动作、分离动作

   * 相交动作规约：首先是非交错规约，且其中存在若干个不同组件且必须同时发生的步骤
   * 分离动作规约：不是相交动作的规约

   > 相交动作规约常用于跨组件通信行为的高度抽象描述
   >
   > 例如线性缓存接口的$Send$和$Reply$约定

### 10.6 活性和隐藏

#### 10.6.1 活性和闭包

**活性条件**

* 通过在独立组件上添加公平性条件，即可为组合规约指定活性条件

* 常见问题：选择哪些变量作为下标，可以选择组件的变量$v_k$，也可以选择全变量$v$

  * 何时需要做出选择：安全部分允许出现$v$改变，而$v_c$不变的步骤

  * 选择什么作为下标：回顾$WF_v(A)$弱公平性公式：

    $\square(\square\text{ENABLED}(A) \; \Rightarrow \; \Diamond<A>_v$

    如果选择$v$，则$<>$符号可以被$v$中的任一个变量更改所满足

    故一般选择$v_c$作为下标

**闭包条件**

* 问题：如果每个组件的规约是闭包，则组合规约是否必然是闭包？
  * 初步结论：如果正常编写规约，则得到的是闭包

#### 10.6.2 隐藏变量

考虑两个规约的组合，$h$为一个记录，在两个组件规约中都有出现，其两个字段中： $h.c_1$只在$S_1$中访问，$h.c_2$只在$S_2$中访问，则有：

$\boldsymbol{\exists}h:\; S_1 \;\wedge\; S_2 \quad \equiv \quad (\boldsymbol{\exists} h_1: T_1) \;\wedge\; (\boldsymbol{\exists}h_2 :\; T_2) $ 

**组合隐藏规则(看不懂，摆着先#TODO)**

若变量$h$不在公式$T_i$中出现，且$S_i$是由$T_i$通过用$h[i]$代换$q$得到的，则对任意有限集$C$，有：

$\boldsymbol{\exists} h:\; \forall i \; \in \; C:\; S_i \quad \equiv \quad (\forall i \;\in\; C: \boldsymbol{\exists}q:\; T_i)$

其蕴含：

$(\boldsymbol{\exists}h :\; \square IsFcnOn(h, C)\;\wedge \; \forall i \;\in\; C:\; S_i) \quad \equiv \quad (\forall i \;\in\; C:\; \boldsymbol{\exists}q:\; T_i)$

### 10.7 开放系统规约

>回顾：开放系统：只描述系统的正确行为
>
>​			封闭系统&完备系统：描述系统及其环境的正确行为

1. 一个由环境规约$E$和系统规约$M$组成的组合规约的形式为$E\;\wedge\;M$

2. 开放系统的作用类似于API,是系统的使用者和实现者的约定，单独的$M$不可用作规约

3. 我们采用$E\; \frac{+}{\;\;} \triangleright M$表示环境$E$和系统$M$组成的开放系统，这个符号的含义为：

   * $E \quad \Rightarrow \quad M$
   * 如果$E$的前$n$个状态未违反安全属性，则$M$的前$(n+1)$个状态也未违反安全属性
   * 在编写开放系统规约时：我们需要将系统的表现和环境的表现分开，且在最高级规约注入系统/环境的固有属性(即不能自然归属的部分)

>开放系统规约和完备系统规约**不等价**

### 10.8 接口转化

* $def$    接口转化(interface refinement)：通过转化  高层级的规约的变量  获得  低层级规约  的一种方法
  * $Example$:   通过第2章的时钟规约实现二进制时钟，通过一个$4-\text{bit}$寄存器，将1-12显示为 0001 ~ 1100
  * $Example$:   通过第3章的通道规约实现一个  在通道上发送数字1-12($Meesage$)的二进制序列来与其环境交互的系统
  * 具体的实现方案如下节所述

#### 10.8.1-2 时钟和通道的例子

**二进制时钟**

1. 为了复用时钟模块，首先需要定义一个 **由新变量(二进制串)** 转换到 **旧变量（十进制数)**的函数
2. 其次为了拓宽定义域，需要在其上套一层$IF \; \dots \; THEN \; \dots \; ELSE $ 以拓宽定义域
3.  为了对外隐藏旧变量，需要在最终规约中使用时态存在量词$\boldsymbol{\exists}$，并使用一个转换变量函数$IR$
   * 最终高级规约书写为 $\boldsymbol{\exists} old:\; IR(old, \; new) \quad \wedge \quad \dots \dots $的形式
   * $IR$的书写为 $IR(new,\; old)\quad \triangleq \quad \square(old \; = \; Trans(new))$
   * 数据流：$old$外部输入，在$Trans$封装的条件语句结构中转化为对应旧形式的变量值，最终输入到旧的规约中

**二进制通道**

1. $\boldsymbol{\exists}h:\; IR \; \wedge \; HSpec$  即为常用的最高级规约形式
2. 确定$IR$的具体写法
   * 注意二进制通道和二进制时钟的区别：
     * 二进制时钟中：一个时钟的十进制读数与二进制读数是一对一映射，即状态的映射也是11对应的
     * 二进制通道中：高级通道(发送十进制数字) 对应四个二进制数字，即状态映射是1对多的
   * $IR$ 应当具有的性质：
     * 记高级通道为$h$，低级通道为$l$
     * 首先，同二进制时钟相似的，$IR$应当将新变量转化为旧变量，在这里即为：将$h$上发送的序列作为$l$上发送值的函数
     * 类比拓宽定义域的形式，我们需要定义当$l$发送不合法二进制序列值时，$h$上发送的值也不合法——即不为$1-12$
     * 务必注意，在上一条中“拓宽定义域”的实质为**不对 $l$ 做出限制**
     * 由于是"4对1"形式，故$IR$实际上也为一个状态机，即也应当具有初始条件和后继条件动作
     * 在$IR$中引入内部变量，代表目前为止接收到的二进制序列
     * 具体实现部分详见书图10.4

#### 10.8.3 接口转化推广

我们的目标：通过高层级规约 $HSpec$ 实现低层级规约$LSpec$

1. 我们给出的最高级规约：$LSpec \quad \triangleq \quad \boldsymbol{\exists}h:\; IR \;\wedge\;HSpec$

   * $h$是$HSpec$的自由变量(在这里自由变量的含义为：外部可见)
   * $IR$表示 $h$ 和低层级$LSpec$的变量 $l$ 的关系
   * 我们可以将内部规约$IR \;\wedge\; HSpec$  视为两个组件的组合，如下图：

   <img src="./data/2.png"/>

   * $IR$可视为组件规约，将 $l$ 的低层级行为转换为 $h$ 的高层级行为

2. $def$    接口转化(interface refinement)：上方的公式$IR$即称为接口转化，将 $l$ 转化为 $h$

3. 对于一组变量，规约即为：

   * $LSpec \quad \triangleq \quad \boldsymbol{\exists}h_1, \; h_2\dots\; h_n:\; IR \;\wedge\;HSpec$

4. $def$    数值转化(data refinement)：$IR$ 形容 $\square P$, $P$ 为一状态谓词，将高级变量表示为低级变量的函数

   * 二进制时钟即为数值转化
   * 数值转化是接口转化的最简单形式
   * 若不能将高级变量表示为低级变量的函数，如前述二进制通道规约所示，高级通道所发送的值取决于当前低级通道状态和先前低级通道所发送的bit值

#### 10.8.4 开放系统的接口转化

* 略 #TODO

****



## 11 高级示例

将规约进行分类：

1. 按照是否具有 变量声明(VARIABLE)的原则将规约分类：
   * 没有变量的规约： 定义了  **数据结构** 及 基于这些数据结构的运算
   * 具有变量的规约：描述系统的规约
2. 将系统规约分为高层级和低层级两类
   * 高层级规约： 描述系统行为的正确性准则
   * 低层级规约： 描述系统的实际行为

### 11.1 定义数据结构

#### 11.1.1 局部定义

$LOCAL$ 运算符 作用于定义和$INSTANCE$  模块实例化中，如下写法

* $LOCAL \; Foo(x) \quad \triangleq \quad \dots$

* $LOCAL  \; INSTANCE \; Sequences$

* 特别注意，不能书写$LOCAL \; EXTENDS \; Sequences$，

  另外：实际上$EXTENDS \; Sequences$  和  $INSTANCE \; Sequences$是等价的

采用 $LOCAL$ 修饰符的定义，引入该模块或实例化中将无法获取定义，即作用范围被严格限制在当前模块中

#### 11.1.2 图

> 如书图11.1，定义了$Graphs$包，代码书写中有部分注意事项如下：

1. 引入的包均需用$LOCAL$  关键字修饰

2. 图11.1定义的各种方法，如判断是否为有向图等，均需要有$G$入参，也即：数据结构包不能包含变量

3. 实际上，如针对单个图我们可以引入 **CONSTANT**常量，在引用模块时作实例化操作；但对于运算符等，定义接口似乎更合适

4. 在编写模块前，需要思考采用何种数据结构定义图，这里采用记录，"nodes"字段对应点的Sequences， "edges"对应边的二元组对应的Sequences，合理使用$\to$ 和 $\longmapsto$ 

5. 书中按照顺序定义的运算符：

   * IsDirected(G)   是否是有向图 (结果为布尔值)
   * DirectedSubGragph(G)  G的所有子图集合(结果为集合)
   * IsUndirectedGragph(G)  是否是无向图(结果为布尔值)(无向图即包含边及其反相边的有向图)
   * UndirectedSubGrapgh(G)  G的无向子图集合(结果是集合)
   * Path(G) G的路径的集合(结果是集合)(注意TLA中可出现无限集合)
   * AreConnectedIn(m, n, G)  G中的m和n是否连通 (结果为布尔值)
   * IsStronglyConncted(G) G是否是强连通图(结果为布尔值)
   * IsTreeWithRoot(G, r)  G是否是以r为根节点的树

   >  该定义与BUAA OO课程第三单元中的 JML 有许多类似之处

#### 11.1.3 求解微分方程

> 定义了求解微分方程的包，通过定义高阶导数等实现了微分方程的引入
>
> 该包的定义详见9.5节描述

1. 首先给出9.5节的$Integrate$运算符的形式化表述：
   * $Intergrate(D,\;a,\;b,\;InitVals) \quad = \quad \langle f^{(0)}[b],\;\dots\;,\; f^{(n-1)}[b]   \rangle$              式中$f^{(0)}$为函数$f$本身
   * $f$ 满足：
     * $\forall r \in(a,b):\; D[r,\; f^{(0)}[r],\;\dots\;,\; f^{(n)}[r]] \quad = \quad 0$
     * $\langle f^{(0)}[a],\;\dots\;,\; f^{(n-1)}[a]   \rangle = InitVals$

2. 为形式化定义上述描述，我们引入另一个函数 $g$ 以消除$f$ 右上角的导数阶数标识

   * $g$ 的定义域为$\{0, \; 1, \; \dots \; n\}$， 映射的值域为函数 $f$有关的导函数

     * $g[0]$即为微分方程的解，对应的目标函数
     * $g[i] = f^{(i)}$ 即目标函数的 $i$ 阶导数

   * 故我们有 $g$ 应当满足的 对应$f$的条件：

     * $\exists e \in Real:\;$ 上述关于$f$条件在$(a-e, b+e)$成立(开区间扩展到闭区间) 
     * $\forall r \in(a-e,b+e):\; D[r,\; g[0][r],\;\dots\;,\; g[n][r]] \quad = \quad 0$
     * $\langle g[0][a],\;\dots\;,\; g[n-1][a]   \rangle = InitVals$

   * 为了实现以上条件以声明$g$, 我们需要准备以下运算符：

     * 对$g$ 是 $f$ 导函数： 需要定义导函数预算符$IsDeriv$， $IsDeriv(i, g, f)$为真当且仅当$g = f^{(i)}$，再通过  $CHOOSE$ 运算符可以实现对导函数的声明
     * 为了实现高阶导数，直接的想法是利用  递归， 采用一个一维函数记录各阶导数即可，这要求我们给出一阶导数的形式化定义即可
     * $D$是多入参的函数，参考第5章中对于多入参函数的说明，我们只需要改写为 元组 形式，采用$Sequences$包中的  $\circ$  运算符，配合$\forall i \; \in \dots$即可

   * 如上条分析，为了实现一阶导函数的定义，我们有：

     * 首先复习$\epsilon - \delta$语言对于导数的表述：$def \quad f'(x): \quad\forall \epsilon > 0:\; \exists \delta > 0 \;\;s.t. |\frac{f(x+\delta) - f(x)}{\delta} - f'(x)| < \epsilon$
     * 为了消除绝对值，我们需要定义$Nbhd$:  $Nbhd(r, \delta) \quad \text{即为} \quad (r-\delta, \;r + \delta)$
     * 即有($PosReal$ 为正实数集合)：

     $\begin{flalign} & \forall \epsilon \in PosReal:\\&\qquad \exists \delta \in PosReal:\\ &\qquad \quad \forall s \in Nbhd(r, \delta) - \{r\}:\; \frac{f[s]-f[r]}{s-r} \in Nbhd(df[r], \epsilon) \end{flalign}$

   * 具体$TLA^+$实现详见书11.1.3 图11.2

#### 11.1.4 BNF语法

> 通过$TLA^+$语言描述$BNF$范式语法所描述的语言规范
>
> 这涉及到一种"转换"，具体来讲类似于在$TLA^+$中定义一种字符串处理工具

1. 我们的目标：将$BNF$范式所描述的语法规则采用$TLA^+$语言描述
   * 具体而言，我们需要将$BNF$范式和$TLA^+$语言间建立起转化规则，或：用什么$TLA^+$方法表示$BNF$范式
   * 我们将$BNF$范式表述的内容视为"字符串" (注：$TLA^+$语言没有类型，也即没有字符串类型 )，我们采用$Sequences$包将其表述
2. 表述$BNF$范式中运算符的方法
   * 自然连接：虽然在范式中，这一操作很自然，但$TLA^+$中并没有相关定义，需要我们将其转化为一个运算符，采用 $\circ$ 符号
   * 或运算符 ( | )： 采用集合中的并集运算 即  $\cup$  符号
   * 其他运算符： $Nil$、 $L+$、 $L^*$
3. 最终表述语法的最高级表述：
   * 我们要寻找的是满足要求的最小语法$G$： 采用$CHOOSE$运算符，选取的最终的语法是所有满足条件语法里的最小集，(选一个语法集，其是任意满足条件语法集合的子集)
   * 翻译每一条语法规则：最终进行合取
   * 其他：为了翻译语法规则等，需要定义辅助预算符，具体见书11.1.4节的图11.3-11.4

### 11.2 其他内存系统规约

> 在本节中，我们将利用所学的大多数$TLA^+$知识书写更真实，也即更高级的内存系统规约
>
> 重点为11.2.3小节中的可串行内存系统，其规约的复杂程度是本书之最
>
> 我们的顺序大致为：抽象系统的共同特征——确定系统描述的实现细节——自顶级规约向下逐步书写规约
>
> * 目标的语文描述：定义一个允许多个未决请求和更弱正确性条件的正确性条件的内存系统，并且为了简化规约，仅考虑对单个内存字的 读、写 操作

#### 11.2.1 接口

> 定义接口，即确定我们在$TLA^+$中描述整个系统的数据结构和整体逻辑
>

接口处我们可以给出关于寄存器的类型正确不变式

1. 如第五章所述，我们描述的是多处理器系统，首先需要有多个$Proc$
2. 真实的内存系统中：处理器是通过 设置寄存器 提交对内存系统的访问需求的，故我们需要有单个寄存器下辖的寄存器集合$Reg$
3. 考虑每个寄存器内的字段：即相当于第五章内每个$Proc$所对应的各种状态，这里我们可以根据寄存器的不同字段给出定义：
   * $adr$    寻址信息
   * $val$    内存字的取值(读操作的"返回值"， 写操作的“写入值")
   * $op$      当前进行中的操作类型

#### 11.2.2正确性条件 

> 如本节开始所述，相对于第五章我们要弱化正确性要求，故这里我们语义式地给出所需的正确性需求
>
> \#TODO 其实没太看懂

1. 原始的正确性条件

   所有处理器的各种操作均按照某种特定顺序执行，使任何执行结果均类似，且每个操作的执行发生在请求和响应之间

2. 修改后的条件

   所有处理器的各种操作均按照某种特定顺序执行，使任何执行结果均类似，且单个处理器的操作严格按照请求提交的顺序执行

3. 几个关键问题：

   * 能否预测未来(如果不能，必须用已有的写操作解释后续的读操作)

#### 11.2.3 串行内存系统

> 不能预测未来且不能修改其说明的内存系统

1. 首先定义系统的后继状态动作
   $$
   \begin{flalign}
   & \vee\; \exists proc \in Proc, \; reg \in Reg:\\
   & \qquad \vee \; \exists req \in Request:\; IssueRequest(proc,\; req,\; reg)\\
   & \qquad \vee \; RespondToRequest(proc,\;reg)\\
   & \vee \; Internal
   \end{flalign}
   $$

   * $IssueRequest(proc,\;req,\; reg)$    处理器$proc$通过寄存器$reg$发出请求$req$的动作
   * $RespondToRequest(proc, reg)$    系统响应处理器$proc$的寄存器$reg$中请求的动作
   * $Internal$     仅改变内部状态的动作

2. 

#### 11.2.4 顺序一致内存系统



****



## 12 语法分析器



****



## 13 TLATEX 排版器



****



## 14 TLC模型检查器



****



